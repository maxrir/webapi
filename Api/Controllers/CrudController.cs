﻿using DataBase.Connection;
using Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Api.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class CrudController : ApiController
    {
        private Repository<Person> _repository;

        public CrudController()
        {
            _repository = new Repository<Person>();
        }
        [HttpGet]
        [Route("api/get/{id}")]
        public Person Get(int id)
        {
            try
            {
                var qry = _repository.GetQuery();
                var res = qry.FirstOrDefault(x => x.Id == id);
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpGet]
        [Route("api/list")]
        public IHttpActionResult GetAll( )
        {
            try
            {
                var qry = _repository.GetQuery();
                var res = qry.ToList();
                return Json(res);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        [Route("api/insert")]
        public IHttpActionResult Post([FromBody]Person person)
        {
            try
            {
                _repository.Insert(person);
                _repository.SaveChanges();
                return Json("OK");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPut]
        [Route("api/update")]
        public IHttpActionResult Put([FromBody]Person person)
        {
            try
            {
                _repository.Update(person);
                _repository.SaveChanges();
                return Json("OK");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpDelete]
        [Route("api/delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(Get(id));
                _repository.SaveChanges();
                return Json("OK");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
