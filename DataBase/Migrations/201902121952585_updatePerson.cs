namespace DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatePerson : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "Sexo", c => c.String(nullable: false, maxLength: 8000, fixedLength: true, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.People", "Sexo");
        }
    }
}
