// <auto-generated />
namespace DataBase.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class alturaOpicionalNull : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(alturaOpicionalNull));
        
        string IMigrationMetadata.Id
        {
            get { return "201902131957599_alturaOpicionalNull"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
