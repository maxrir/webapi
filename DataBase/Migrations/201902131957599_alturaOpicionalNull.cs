namespace DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class alturaOpicionalNull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.People", "Altura", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.People", "Altura", c => c.Double(nullable: false));
        }
    }
}
