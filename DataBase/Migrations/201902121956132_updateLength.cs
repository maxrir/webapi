namespace DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.People", "Sexo", c => c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.People", "Sexo", c => c.String(nullable: false, maxLength: 8000, fixedLength: true, unicode: false));
        }
    }
}
