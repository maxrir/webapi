﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Models.Model
{
    [Table("People")]
    public class Person
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public int Idade { get; set; }
        [Required]
        [Column(TypeName = "char")]
        [MaxLength(1)]
        public string Sexo { get; set; }
        [Required]
        public double Peso { get; set; }
        public double Altura { get; set; }
    }
}